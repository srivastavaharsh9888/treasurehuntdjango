from django.views.generic import TemplateView
from django.contrib import messages
from django.shortcuts import render,HttpResponse,redirect
from accounts.models import Question, Soln, Participants,Hint

class IndexPageView(TemplateView):
    template_name = 'main/index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        par_obj=Hint.objects.all()
        par2_obj=Participants.objects.all()

        if par_obj.count()!=0:
        	context['hints'] = par_obj
        	context['show']=True
        else:
        	context['data'] = "We will soon update some hint. Till that time try it by yourself.."
        	context['show']=False


        context['scores'] = par2_obj

        return context


class TreasureHuntView(TemplateView):
    template_name = 'main/hunt.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.user.is_authenticated:
	        par_obj=Participants.objects.get(user=self.request.user)
	        try:
	        	context['question'] = Question.objects.get(id=int(par_obj.score)+1).question
	        	context['show']=True
	        except:
	        	context['question'] = "Seems like You have finished all the questions wait for the results then."
	        	context['show']=False
        return context

class ChangeLanguageView(TemplateView):
    template_name = 'main/change_language.html'

def SubmitAnswer(request):
	if request.method=="POST":
		print(request.user)
		ans=request.POST.get("solution")
		par_obj=Participants.objects.get(user=request.user)
		Soln.objects.create(user=request.user,answer=ans)
		solutions=Question.objects.get(id=int(par_obj.score)+1).answer.split(",")
		if ans in solutions:
			par_obj.score=par_obj.score+1
			par_obj.save()
			return redirect('/quiz/')
		else:
			messages.warning(request,"Sorry, Try Again...")
	return redirect('/quiz/')
